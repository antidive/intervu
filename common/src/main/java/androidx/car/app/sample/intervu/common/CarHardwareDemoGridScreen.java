/*
 * Copyright (C) 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package androidx.car.app.sample.intervu.common;

import static androidx.car.app.CarToast.LENGTH_LONG;
import static androidx.car.app.model.Action.BACK;
import static androidx.car.app.model.CarColor.GREEN;
import static androidx.car.app.model.CarColor.RED;

import android.content.res.Resources;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.car.app.CarContext;
import androidx.car.app.CarToast;
import androidx.car.app.Screen;
import androidx.car.app.hardware.CarHardwareManager;
import androidx.car.app.hardware.common.CarValue;
import androidx.car.app.hardware.common.OnCarDataAvailableListener;
import androidx.car.app.hardware.info.CarInfo;
import androidx.car.app.hardware.info.CarSensors;
import androidx.car.app.hardware.info.EnergyLevel;
import androidx.car.app.hardware.info.Speed;
import androidx.car.app.model.Action;
import androidx.car.app.model.ActionStrip;
import androidx.car.app.model.CarColor;
import androidx.car.app.model.CarIcon;
import androidx.car.app.model.GridItem;
import androidx.car.app.model.GridTemplate;
import androidx.car.app.model.ItemList;
import androidx.car.app.model.Template;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.IconCompat;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;

import java.util.concurrent.Executor;

/** Creates a screen that demonstrates usage of the full screen {@link GridTemplate}. */
public final class CarHardwareDemoGridScreen extends Screen implements DefaultLifecycleObserver {

    @Nullable
    private IconCompat mSpeedIcon;

    @Nullable
    private IconCompat mBatteryIcon;

    private CarColor mEnergyLevelColor;

    @Nullable
    Speed mSpeed;
    @Nullable
    EnergyLevel mEnergyLevel;
    private boolean mHasSpeedPermission;
    private boolean mHasEnergyLevelPermission;
    private MutableLiveData<String> speedStr;
    private String mSpeedInfoStr;
    private String mEnergyLevelLowFuelStr;
    private String mEnergyLevelRangeStr;
    private String mEnergyLevelFuelLvlStr;
    private final CarContext mCarContext;

    private final Executor mCarHardwareExecutor;

    private OnCarDataAvailableListener<Speed> mSpeedListener = data -> {
        synchronized (this) {
//            Log.i(TAG, "Received speed information: " + data);
            mSpeed = data;

            // Prepare text for Speed
            StringBuilder info = new StringBuilder();
            if (!mHasSpeedPermission) {
                info.append("No Speed Permission.");
            } else if (mSpeed == null) {
                info.append("Fetching Speed.");
            } else {
                if (mSpeed.getDisplaySpeedMetersPerSecond().getStatus()
                        != CarValue.STATUS_SUCCESS) {
                    info.append(" ");
                } else {
                    float roundOff = (float) 3.6 * Math.round(mSpeed.getDisplaySpeedMetersPerSecond().getValue());
                    info.append(roundOff);
                    info.append(" km/h");
                }
            }
            mSpeedInfoStr = info.toString();
            invalidate();
        }
    };

    private OnCarDataAvailableListener<EnergyLevel> mEnergyLevelListener = data -> {
        synchronized (this) {
//            Log.i(TAG, "Received energy level information: " + data);
            mEnergyLevel = data;

            // Prepare text for Energy Level
            StringBuilder info = new StringBuilder();
            if (!mHasEnergyLevelPermission) {
                info.append("No EnergyLevel Permission.");
            } else if (mEnergyLevel == null) {
                info.append("Fetching Energy Level.");
            } else {
                if (mEnergyLevel.getEnergyIsLow().getStatus() != CarValue.STATUS_SUCCESS) {
                    mEnergyLevelLowFuelStr = "N/A";
                } else {
                    mEnergyLevelLowFuelStr = mEnergyLevel.getEnergyIsLow().getValue().toString();
                }
                if (mEnergyLevel.getRangeRemainingMeters().getStatus() != CarValue.STATUS_SUCCESS) {
                    mEnergyLevelRangeStr ="N/A";
                } else {
                    StringBuilder range = new StringBuilder();
                    range.append(mEnergyLevel.getRangeRemainingMeters().getValue() / 1000);
                    range.append(" kms");
                    mEnergyLevelRangeStr = range.toString();
                }
                if (mEnergyLevel.getFuelPercent().getStatus() != CarValue.STATUS_SUCCESS) {
                    info.append("Fuel: N/A. ");
                } else {
                    StringBuilder fuelLvl= new StringBuilder();
                    float fuelLvlVal = mEnergyLevel.getFuelPercent().getValue();
                    if (fuelLvlVal > 50) mEnergyLevelColor = GREEN;
                    else mEnergyLevelColor = RED;
                    fuelLvl.append(fuelLvlVal);
                    fuelLvl.append("% ");
                    mEnergyLevelFuelLvlStr = fuelLvl.toString();
                }
            }

            invalidate();
        }
    };

    public CarHardwareDemoGridScreen(@NonNull CarContext carContext) {
        super(carContext);
        getLifecycle().addObserver(this);
        mCarContext = carContext;
        mCarHardwareExecutor = ContextCompat.getMainExecutor(mCarContext);
    }

    @Override
    public void onCreate(@NonNull LifecycleOwner owner) {
        Resources resources = getCarContext().getResources();
        mEnergyLevelColor = GREEN;
        mSpeedIcon = IconCompat.createWithResource(getCarContext(), R.drawable.speed_ico);
        mBatteryIcon = IconCompat.createWithResource(getCarContext(), R.drawable.ic_battery_svgrepo_com);
        mSpeedInfoStr = "0";
        mEnergyLevelRangeStr = "";
        mEnergyLevelFuelLvlStr = "";
        mEnergyLevelLowFuelStr = "";
    }

    @Override
    @SuppressWarnings({"FutureReturnValueIgnored"})
    public void onStart(@NonNull LifecycleOwner owner) {

        CarHardwareManager carHardwareManager =
                mCarContext.getCarService(CarHardwareManager.class);
        CarInfo carInfo = carHardwareManager.getCarInfo();
        CarSensors carSensors = carHardwareManager.getCarSensors();

        mSpeed = null;
        try {
            carInfo.addSpeedListener(mCarHardwareExecutor, mSpeedListener);
            mHasSpeedPermission = true;
        } catch (SecurityException e) {
            mHasSpeedPermission = false;
        }

        mEnergyLevel = null;
        try {
            carInfo.addEnergyLevelListener(mCarHardwareExecutor, mEnergyLevelListener);
            mHasEnergyLevelPermission = true;
        } catch (SecurityException e) {
            mHasEnergyLevelPermission = false;
        }
    }

    @Override
    public void onStop(@NonNull LifecycleOwner owner) {
        CarHardwareManager carHardwareManager =
                mCarContext.getCarService(CarHardwareManager.class);
        CarInfo carInfo = carHardwareManager.getCarInfo();
        try {
            carInfo.removeSpeedListener(mSpeedListener);
            mHasSpeedPermission = true;
        } catch (SecurityException e) {
            mHasSpeedPermission = false;
        }

        mSpeed = null;

        try {
            carInfo.removeEnergyLevelListener(mEnergyLevelListener);
            mHasEnergyLevelPermission = true;
        } catch (SecurityException e) {
            mHasEnergyLevelPermission = false;
        }

        mEnergyLevel = null;
    }

    @NonNull
    @Override
    public Template onGetTemplate() {
        ItemList.Builder gridItemListBuilder = new ItemList.Builder();

        gridItemListBuilder.addItem(
                new GridItem.Builder()
                        .setImage(new CarIcon.Builder(mSpeedIcon).build(), GridItem.IMAGE_TYPE_ICON)
                        .setTitle("Speed")
                        .setText(mSpeedInfoStr)
                        .build());

        gridItemListBuilder.addItem(
                new GridItem.Builder()
                        .setImage(new CarIcon.Builder(mSpeedIcon).build(), GridItem.IMAGE_TYPE_ICON)
                        .setTitle("Range")
                        .setText(mEnergyLevelRangeStr)
                        .build());

        gridItemListBuilder.addItem(
                new GridItem.Builder()
                        .setImage(new CarIcon.Builder(mBatteryIcon).setTint(mEnergyLevelColor).build(), GridItem.IMAGE_TYPE_ICON)
                        .setTitle("Fuel Level")
                        .setText(mEnergyLevelFuelLvlStr)
                        .build());
        gridItemListBuilder.addItem(
                new GridItem.Builder()
                        .setImage(new CarIcon.Builder(mSpeedIcon).build(), GridItem.IMAGE_TYPE_ICON)
                        .setTitle("Low Fuel Indicator")
                        .setText(mEnergyLevelLowFuelStr)
                        .build());

        return new GridTemplate.Builder()
                .setHeaderAction(Action.APP_ICON)
                .setSingleList(gridItemListBuilder.build())
                .setTitle("Grid Template Demo")
                .setActionStrip(
                        new ActionStrip.Builder()
                                .addAction(
                                        new Action.Builder()
                                                .setTitle("Settings")
                                                .setOnClickListener(
                                                        () ->
                                                                CarToast.makeText(
                                                                        getCarContext(),
                                                                        "Clicked Settings",
                                                                        LENGTH_LONG)
                                                                        .show())
                                                .build())
                                .build())
                .setHeaderAction(BACK)
                .build();
    }
}
